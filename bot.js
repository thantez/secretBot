const SDK = require("balebot");
//#region include from SDK
const BaleBot = SDK.BaleBot;
//#endregion
//#region constants
const TOKEN = "a61dedd494d84e3b8a8de668f0c006b68aca6d5c";
let bot = new BaleBot(TOKEN, {
    log: {
        enabled: true,
        level: "INFO" // other options: "TRACE", "DEBUG", "WARN", "ERROR", "FATAL"
    },
    requestQueue: {
        fetchInterval: 0, // in ms. the time between sending two consecutive requests.
        retryInterval: 0, // in ms. the time to wait before resending a failed request.
        timeout: 30000, // in ms. the time period to try for sending each request. if the request failed again after this time it will be rejected with the "TIME_OUT" message.
    },
    socket: {
        reconnectInterval: 30000 // in ms. when the socket disconnects, waits as much as this time and the tries to reconnect.
    }
});
bot.hears('/start', (mes, user) => {
    user.reply('hi. you can start message conversation with ');
})
bot.setDefaultCallback((mes, res) => {
    res.reply('bot is under creation, please wait! :)');
});